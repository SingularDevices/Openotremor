/* 
     
      __   ____     ___  ____    ___   ______  ____     ___  ___ ___   ___   ____  
    /   \ |    \   /  _]|    \  /   \ |      ||    \   /  _]|   |   | /   \ |    \ 
   |     ||  o  ) /  [_ |  _  ||     ||      ||  D  ) /  [_ | _   _ ||     ||  D  )
   |  O  ||   _/ |    _]|  |  ||  O  ||_|  |_||    / |    _]|  \_/  ||  O  ||    / 
   |     ||  |   |   [_ |  |  ||     |  |  |  |    \ |   [_ |   |   ||     ||    \ 
   |     ||  |   |     ||  |  ||     |  |  |  |  .  \|     ||   |   ||     ||  .  \
    \___/ |__|   |_____||__|__| \___/   |__|  |__|\_||_____||___|___| \___/ |__|\_|                                                                               
   
   Openotremor  V 0.01
   Opensource Parkinson's tremor cancellation device  |  Experimental Concept Test
   
   Based on prototype invented by Haiyan Zhang on public video clip episode 1 of BBC program  Big Life Fix day 7 December 2016
   http://www.bbc.co.uk/programmes/p04jylxm   

   Sciencie about tremor cancellation 
   http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0112782

       
   WARRANTY DISCLAIMER. YOU AGREE THAT YOU ARE USING THIS EXPERIMENTAL SOFTWARE AND HARDWARE DIAGRAM SOLELY AT YOUR OWN RISK. AUTOR PROVIDES 
   THE SOFTWARE  “AS IS” AND WITHOUT WARRANTY OF ANY KIND,  YOU ACKNOWLEDGE THAT AUTOR EXPRESSLY DISCLAIMS USE OF THE SOFTWARE FOR  ANY MEDICAL
   PURPOSE (INCLUDING, WITHOUT LIMITATION, THE DIAGNOSIS, EXAMINATION, OR TREATMENT OF ANY  MEDICAL CONDITIONS).
   AUTOR DISCLAIMS ANY REPRESENTATIONS OR WARRANTIES REGARDING THE SOFTWARE MADE BY YOU OR ANY THIRD PARTY  AND ANY SUCH REPRESENTATION OR
   WARRANTY IS NOT MADE ON AUTOR’S BEHALF. 
   

   Copyright (C) 2017   Singular Devices Maker Studio            gbarbarov@singulardevices.com

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  
*/


/* 
 external commands serial port 9600 baud
 
  A\r                     activate all actuators
  D\r                     desactive all actuators
  +\r                     increment dtime 10 ms
  -\r                     decrement dtime 10 ms
  C\r                     clear pattern
  B\r                     BBC demo pattern
  P,<1-6>,<1-20>,<0-1>\r  set point pattern    
  R\r                     print pattern
  
  (to do)
  *M\r                     Memorize pattern on eeprom 
  *G\r                     Gaussian noise pattern
  *S\r                     Stochastic noise pattern 
  *I\r                     IMU sensor read 
  *T\r                     auto-tunning mode
  *N\r                     normal mode
  *An Android APP debug tool
  
 */

/*     
  BOM Openotremor Hardware V 1
  -Arduino Nano or clone    
  -ULN 2803  power driver (or easy adapt to ULN2003 ) 
  - 6 x vibration micro-motor    
  -(optional) Serial Bluetooth Module for debug stage 
  
  Circuit diagram
 
                      +-----+
         +------------| USB |------------+
         |            +-----+            |                                                     +----+----+----+----+----+-----+
         | [ ]D13/SCK        MISO/D12[ ] |                               +----\_/----+         |    |    |    |    |    |     | 
         | [ ]3.3V           MOSI/D11[ ]~|-------------+                 | 1       18|        [M1] [M2] [M3] [M4] [M5] [M6]   |
         | [ ]V.ref     ___    SS/D10[ ]~|-----------+ |                 | 2       17|         |    |    |    |    |    |     | 
         | [ ]A0       / N \       D9[ ]~|---------+ | +-----------------| 3       16|---------+    |    |    |    |    |     | 
         | [ ]A1      /  A  \      D8[ ] |         | +-------------------| 4       15|--------------+    |    |    |    |     |
         | [ ]A2      \  N  /      D7[ ] |         +---------------------| 5       14|-------------------+    |    |    |     |
         | [ ]A3       \_0_/       D6[ ]~|-------------------------------| 6       13|------------------------+    |    |     |
         | [ ]A4/SDA               D5[ ]~|-------------------------------| 7       12|-----------------------------+    |     |
         | [ ]A5/SCL               D4[ ] |                 +-------------| 8       11|----------------------------------+     | 
         | [ ]A6              INT1/D3[ ]~|-----------------+  +----------| 9       10|----------------------------------------+
         | [ ]A7              INT0/D2[ ] |                    |          +-----------+                                        |
+--------| [ ]5V                  GND[ ] |--------------------+             ULN 2803                                          |
|        | [ ]RST                 RST[ ] |                                                                                    |
|        | [ ]GND   5V MOSI GND   TX1[ ] |                                                                                    |
|        | [ ]Vin   [ ] [ ] [ ]   RX1[ ] |                                                                                    |
|        |          [ ] [ ] [ ]          |                                                                                    |
|        |          MISO SCK RST         |                                                                                    |
|        | NANO-V3                       |                                                                                    |
|        +-------------------------------+                                                                                    |
|        ARDUINO NANO                                                                                                         |
|        http://busyducks.com/ascii-art-arduinos                                                                              |
|                                                                                                                             |
+-----------------------------------------------------------------------------------------------------------------------------+

(to do)
- Connect IMU module MPU 6050  to A4 , A5 , +5v , GND
- Change motors by electrodes  and high voltage generator in ULN2803 free pins                                                                                                                 
         
*/


// Pins arduino nano ATmega328 ,(use pin with PWM for future actuator power level option )
#define act_0 3
#define act_1 5
#define act_2 6
#define act_3 9
#define act_4 10
#define act_5 11

#define NACT 6    //max actuators

#define NSTEP 20   //max pulses for pattern

int ttime=2000; // total time pattern milliseconds

int dtime=ttime/NSTEP; //  (Total time / steps ) 
int max_dt=1000; //max time step
int min_dt=10;   //min time step

//constant patterns
//pattern B based on image capture of video clip at 2:00  
byte pat_b[NACT][NSTEP]={ {1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0},
                          {0,1,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,1,0,0},
                          {0,0,0,1,1,0,1,0,1,1,0,1,1,1,0,1,1,0,0,0},
                          {1,0,1,1,0,1,0,0,1,0,1,0,1,0,0,0,1,0,0,0},
                          {1,1,0,1,0,0,1,0,1,0,0,1,0,0,1,1,0,1,0,0},
                          {1,0,0,1,0,1,0,1,0,1,0,1,0,1,0,0,1,0,0,1} };


byte pat_g[NACT][NSTEP]={ {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                          {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                          {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                          {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                          {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                          {0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0} };

byte pat_s[NACT][NSTEP]={ {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                          {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                          {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                          {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                          {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                          {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0} };



byte pattern[NACT][NSTEP];  //dynamic pattern 


// Buffer for data commands
char inData[200];
char *inParse[10];
int index = 0;
boolean CMD_Complete = false;


void refresh(byte x)
{ digitalWrite(act_0,pattern[0][x]);
  digitalWrite(act_1,pattern[1][x]);
  digitalWrite(act_2,pattern[2][x]);
  digitalWrite(act_3,pattern[3][x]);
  digitalWrite(act_4,pattern[4][x]);
  digitalWrite(act_5,pattern[5][x]);
}

void setup() {  
Serial.begin(9600); 
pinMode(act_0,OUTPUT);  
pinMode(act_1,OUTPUT);  
pinMode(act_2,OUTPUT);  
pinMode(act_3,OUTPUT);  
pinMode(act_4,OUTPUT);  
pinMode(act_5,OUTPUT);  

memcpy(pattern,pat_b, NSTEP*NACT*sizeof(byte));  // copy static pattern on dynamic pattern
}

byte x=0;
long int stime=millis();
byte ACT_ON=true;

void active (void)
{ACT_ON=true;
 Serial.println("All on");}

void desactive (void)
{ACT_ON=false;
 digitalWrite(act_0,LOW);
 digitalWrite(act_1,LOW);
 digitalWrite(act_2,LOW);
 digitalWrite(act_3,LOW);
 digitalWrite(act_4,LOW);
 digitalWrite(act_5,LOW);
 Serial.println("All off"); 
}

void dt_inc(int t)
{dtime+=t;
 if (dtime>max_dt) dtime=max_dt;
 Serial.println(dtime); 
}

void dt_dec(int t)
{dtime-=t;
 if (dtime<min_dt) dtime=min_dt;
 Serial.println(dtime); 
}


void clear_patt() 
{for (byte y=0;y<NACT;y++)
  for (byte x=0;x<NSTEP;x++) {pattern[y][x]=0;}                                                                                           
 Serial.println("clear pattern");
}


void print_patt() 
{for (byte y=0;y<NACT;y++)
  {for (byte x=0;x<NSTEP;x++) {Serial.print(pattern[y][x]);};                                                                                           
   Serial.println();
  }
   Serial.println();
}

void set_point(int x,int y ,int val) 
{Serial.print("point (");       
 Serial.print(x+1);
 Serial.print(","); 
 Serial.print(y+1);
 Serial.print(")=");   
 pattern[y][x]=val;     
 Serial.println(val);                              
}


void set_patt(int n) 
{Serial.print("set pattern:");       
 Serial.println(n);  
 switch(n)
 {case 0:memcpy(pattern,pat_b, NSTEP*NACT*sizeof(byte));break; 
  case 1:memcpy(pattern,pat_g, NSTEP*NACT*sizeof(byte));break; 
  case 2:memcpy(pattern,pat_s, NSTEP*NACT*sizeof(byte));break; 
 }
}



void loop() {
if (CMD_Complete) {ParseSerialData();                   
                   CMD_Complete = false;
                  };
  
if ( (millis()-stime)>=dtime) {if (ACT_ON) {refresh(x);};
                               x++;
                               if (x>=NSTEP-1) {x=0;};
                               stime=millis();
                              }                             

}


void serialEvent()
{while (Serial.available() && CMD_Complete == false)
 {char inChar = Serial.read();
  inData[index] = inChar;
  index++;
  if (inChar == '\r') {index = 0;
                       CMD_Complete = true;
                      }
 }
}

void ParseSerialData()
{
char *p = inData;
char *str;
int count = 0;

while ((str = strtok_r(p, ",", &p)) != NULL)
{
inParse[count] = str;
count++;
}

if(count == 1) {char *func = inParse[0];
                switch(*func)
                {case 'A':active() ; break;        
                 case 'D':desactive() ; break;     
                 case '+':dt_inc(10) ; break;
                 case '-':dt_dec(10) ; break;
                 case 'R':print_patt() ; break;
                 case 'C':clear_patt() ; break;
                 case 'B':set_patt(0) ; break;
                 case 'G':set_patt(1) ; break;
                 case 'S':set_patt(2) ; break;
                }
               }

if(count == 4)
              {char *func = inParse[0];
               char *param1 = inParse[1];
               char *param2 = inParse[2];
               char *param3 = inParse[3];
               switch(*func)
               {case 'P': set_point(atoi(param1)-1,atoi(param2)-1,atoi(param3)); break;
               }
              }
}


